//version inicial

var express = require('express'),
  app = express(),
  //port = process.env.PORT || 3000;
  port = process.env.PORT || 2020;

var path = require('path');


app.listen(port);
var bodyparser = require('body-parser')
app.use(bodyparser.json())

app.use(function(req,res, next)
    {
        res.header("Access-Control-Allow-Origin", "*")
        res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Acept");
        next()
    }
)



//Servicios para invocar a Mongo
var requestJson = require('request-json');
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/jmora/collections";


var clienteMLabRaiz = "";
var clienteAltaMLabRaiz = "";
var movimientoAltaMLabRaiz = "";
var movimientoMLabRaiz = "";
var urlClientes = 'https://api.mlab.com/api/1/databases/jmora/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
var clienteMLab = requestJson.createClient(urlClientes);

//-------------------------------//


var movimientosJSON = require('./movimientosv-2.json')


console.log('todo list RESTful API server started on: ' + port);
//Puede ser Get, PUT, Delete...
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/', function (req, res) {
    res.send("Hemos recibido su petición POST CAMBIADA");
})

app.put('/', function (req, res) {
    res.send("Hemos recibido su petición PUT");
})

app.delete('/', function (req, res) {
    res.send("Hemos recibido su petición de Delete");
})

app.get('/clientes/:idcliente', function (req, res) {
    res.send("Aquí tiene al cliente numero: "+req.params.idcliente);
})

app.get('/v1/movimientos/:idcliente', function (req, res) {
    res.sendFile('movimientosv-1.json');
})

app.get('/v2/movimientos', function (req, res) {
    res.json(movimientosJSON);
})

app.get('/v2/movimientos/:id', function (req, res) {
    console.log(req.params.id);
    res.send(movimientosJSON[req.params.id - 1]);
})

app.get('/v2/movimientosQuery/', function (req, res) {
    console.log(req.query);
    res.send("Se recibio el query");
})

app.post('/v2/movimientos', function (req, res) {
    var nuevo = req.body;
    nuevo.id = movimientosJSON.length + 1;
    movimientosJSON.push(nuevo);
    res.send("Movimiento dado de alta")
})

app.get('/Clientes', function (req, res) {
    clienteMLab.get(urlClientes, function (err, resM, body) {
        if(err){
            console.log(body);
        }else{
            res.send(body);
        }
    });
})

app.post('/Clientes', function(req, res) {
    clienteMLab.post('', req.body, function(err, resM, body) {
        res.send(body)
    })
})


app.post('/Login', function(req, res) {
    res.set("Access-Control-Allow-Headers", "Content-Type")
    var email = req.body.email
    var password = req.body.password
    var query = 'q={"email":"'+ email +'","password":"' +password + '"}'
    console.log(query)
    var urlMLab = urlMlabRaiz + "/Clientes?" + query + "&" + apiKey;
    console.log(urlMLab)
    clienteMlabRaiz = requestJson.createClient(urlMLab)
    clienteMlabRaiz.get('', function(err, resM, body) {
        if (!err) {
            if (body.length == 1) { //Login ok, se encontro 1 documento
                //res.status(200).send('Usuario logado')
                res.status(200).send(body[0])
            } else { //No se encontro al usuario
                res.status(404).send('Usuario no encontrado')
            }
        }
    })
})

app.post('/Movimientos', function(req, res) {
    res.set("Access-Control-Allow-Headers", "Content-Type")
    var clienteId = req.body.clienteId
    var query = 'q={"clienteId":'+clienteId+'}'
    var urlMLab = urlMlabRaiz + "/Movimientos?"+query+"&"+apiKey;
    console.log(urlMLab)
    movimientoMLabRaiz = requestJson.createClient(urlMLab)
    movimientoMLabRaiz.get('', function(err, resM, body) {
        if (!err) {
            if (body.length > 0) { //Al menos se encontro un movimiento
                res.status(200).send(body)
            }else if(body.length === 0){
                res.status(200).send(body)
            }
            else { //No se encontraron movimientos
                res.status(404).send('No existen movimientos')
            }
        }
    })
})

app.post('/AltaMovimientos', function(req, res) {
    res.set("Access-Control-Allow-Headers", "Content-Type")
    var clienteId = req.body.clienteId
    var query = 'q={"clienteId":'+clienteId+'}'
    var urlMLab = urlMlabRaiz + "/Movimientos?l=1&s={idMovimiento:-1}&"+query+"&"+apiKey;
    var urlMLabMov = urlMlabRaiz + "/Movimientos?"+apiKey;
    var idMomivientoSec = 0

    var d = new Date();
    var fechaMovimiento = d.getDate()  + "-" + (d.getMonth()+1) + "-" + d.getFullYear() + " " +
        d.getHours() + ":" + d.getMinutes();

    movimientoMLabRaiz = requestJson.createClient(urlMLab)
    movimientoMLabRaiz.get('', function(err, resM, body) {
        if (!err) {
            console.log("Secuencia Movimiento: "+body.length)
            if (body.length > 0) { //Al menos se encontro un movimiento
                idMomivientoSec = body[0].idMovimiento +1
                req.body.idMovimiento = idMomivientoSec
                req.body.fecha = fechaMovimiento
                console.log("Secuencia nuevo movimiento: "+req.body.idMovimiento)
                movimientoAltaMLabRaiz = requestJson.createClient(urlMLabMov)
                movimientoAltaMLabRaiz.post('', req.body,function(err, resM, body) {
                    if (!err) {
                        if (body!=null) { //Al menos se encontro un movimiento
                            res.status(200).send(body)
                        } else { //No se encontraron movimientos
                            res.status(404).send('No fue posible registrar el movimiento')
                        }
                    }
                })
            }else{
                idMomivientoSec = 1
                req.body.idMovimiento = idMomivientoSec
                req.body.fecha = fechaMovimiento
                console.log("Secuencia nuevo movimiento: "+req.body.idMovimiento)
                movimientoAltaMLabRaiz = requestJson.createClient(urlMLabMov)
                movimientoAltaMLabRaiz.post('', req.body,function(err, resM, body) {
                    if (!err) {
                        if (body!=null) { //Al menos se encontro un movimiento
                            res.status(200).send(body)
                        } else { //No se encontraron movimientos
                            res.status(404).send('No fue posible registrar el movimiento')
                        }
                    }
                })
            }
        }else{
            res.status(404).send('No existe Usuario para asociar al Movimiento')
        }
    })
})


app.post('/AltaUsuario', function(req, res) {
    res.set("Access-Control-Allow-Headers", "Content-Type")
    var urlMLab = urlMlabRaiz + "/Clientes?l=1&s={codCliente:-1}&"+apiKey;
    var urlMLabAlta = urlMlabRaiz + "/Clientes?"+apiKey;
    var idNuevoCliente = 0;
    console.log(urlMLabAlta)

    clienteMlabRaiz = requestJson.createClient(urlMLab)
    clienteMlabRaiz.get('', function(err, resM, body) {
        //Id Cliente Nuevo
        if (!err) {
            if (body.length == 1) { //Login ok, se encontro 1 documento
                console.log("ID MAX: "+body[0].codCliente)
                idNuevoCliente = body[0].codCliente+1

                req.body.codCliente = idNuevoCliente
                clienteAltaMLabRaiz = requestJson.createClient(urlMLabAlta)
                clienteAltaMLabRaiz.post('', req.body, function(err, resM, body) {
                    //Id Cliente Nuevo
                    if (!err) {
                        if (body != null) { //Login ok, se encontro 1 documento
                            res.status(200).send(body)
                        } else { //No se encontro al usuario
                            res.status(404).send('No fue posible dar de alta al usuario')
                        }
                    }
                })
            } else { //No se encontro al usuario
                idNuevoCliente = idNuevoCliente+1
                req.body.codCliente = idNuevoCliente
                clienteAltaMLabRaiz = requestJson.createClient(urlMLabAlta)
                clienteAltaMLabRaiz.post('', req.body, function(err, resM, body) {
                    //Id Cliente Nuevo
                    if (!err) {
                        if (body != null) { //Login ok, se encontro 1 documento
                            res.status(200).send(body)
                        } else { //No se encontro al usuario
                            res.status(404).send('No fue posible dar de alta al usuario')
                        }
                    }
                })
            }
        }
    })
})

app.post('/Eliminar', function(req, res) {
    res.set("Access-Control-Allow-Headers", "Content-Type")
    var Id = req.body.movimientoId
    var urlMLab = urlMlabRaiz + "/Movimientos/"+Id+"?"+apiKey;
    console.log(urlMLab)
    movimientoMLabRaiz = requestJson.createClient(urlMLab)
    movimientoMLabRaiz.delete('', function(err, resM, body) {
        if (!err) {
            if (body != null) { //Al menos se encontro un movimiento
                res.status(200).send(body)
            } else { //No se encontraron movimientos
                res.status(404).send('No Fue posible eliminar el movimiento')
            }
        }
    })
})

app.post('/TipoCambio', function(req, res) {
    res.set("Access-Control-Allow-Headers", "Content-Type")
    var token ='?token=99701e41198acda207853d2b6912ecdb7158bde16df4a028d9a0785beef503e0'
    var d = new Date();
    var fecha = d.getFullYear()  + "-" + (d.getMonth()+1) + "-" + d.getDate()
    var hora = d.getHours();
    var fechaEncript = ''

    if(hora<12){
        d = d.getDate()-1
        fecha = d.getFullYear()  + "-" + (d.getMonth()+1) + "-" + d.getDate()
        console.log("Date: " + d)
    }


    var urlBaxico ='https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718/datos/'+fecha+'/'+fecha+token
    console.log(urlBaxico);
    clienteMlabRaiz = requestJson.createClient(urlBaxico)
    clienteMlabRaiz.get('', function(err, resM, body) {
        if (!err) {
            if (body!=null) { //OK Tipo de cambio
                res.status(200).send(body)
                console.log("Hora Actual: "+ hora)
                var hw = encrypt(body.bmx.series[0].datos[0].fecha.toString())
                console.log(hw)
                console.log(decrypt(hw))



            } else { //No se encontro al usuario
                res.status(404).send('Usuario no encontrado')
            }
        }
    })
})




const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const key = crypto.randomBytes(32);
const iv = crypto.randomBytes(16);

function encrypt(text) {
    let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
}

function decrypt(text) {
    let iv = Buffer.from(text.iv, 'hex');
    let encryptedText = Buffer.from(text.encryptedData, 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}

/*
var hw = encrypt("Saludos desde Mexico")
console.log(hw)
console.log(decrypt(hw))
*/
