#Imagen base
FROM node:latest

#Directorio de la app en el contenedor
WORKDIR /app

#Copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#Puerto a exponer
EXPOSE 3000

#Variables de entorno

#Comando para ejecutar el servicio
CMD ["npm", "start"]